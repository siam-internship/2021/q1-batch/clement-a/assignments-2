import React from 'react'
import './footer.css'
import ViewModuleIcon from '@material-ui/icons/ViewModule';

function Footer({setName}) {
    return (
        <div className="footer">
            <div className="footerBottom">
            <p className="drinks" onClick={() => 
                {setName("Drinks")}}>
                    Drinks</p>
            <p className="lunch" onClick={() => 
                {setName("Lunch")}}>Lunch</p>
            <p className="desserts"
            onClick={() => 
                {setName("Desserts")}}>Desserts</p>
            <p className="fruits"
            onClick={() => 
                {setName("Fruits")}}>Fruits</p>
            <p className="snacks"
            onClick={() => 
                {setName("Snacks")}}>Snacks</p>
            <p className="pizza"
            onClick={() => 
                {setName("Pizza")}}>Pizza</p>
            <p>
            <ViewModuleIcon />
            </p>
            
            </div>
            
        </div>
    )
}

export default Footer

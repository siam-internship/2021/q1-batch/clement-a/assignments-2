import './left.css'
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import Data from './data.json'
import React from 'react'

// class Left extends Component {
//     constructor(props) {
//         super(props)
    
//         this.state = {
//              count: 0,
//              name:[]
//         }
//     }
//     incrementCount =() => {
//         this.setState(prevCount=>({
//             count : prevCount.count + 1
//         }    
//         ))
//         alert("incremented");
//     }
    
    
//     render() {
//         return (
//             <div className="root">
//             <div>
//                 <div>
//                     <MenuIcon className="menuicons"/>
//                 </div>
//                 <div className="displayTitle">
//                     Display
//                 </div>
//                 <div>
//                     <SearchIcon className="searchicons"/>
//                 </div>
//             </div>
//             <div className="datas">
//             {
//             Data.map(datas =>(
//                 <div key={datas.product_id}>
//                 <img src={datas.images}  className="imagesdata" onClick={this.incrementCount}/>
//                 <strong>{datas.name}</strong> 
//                 {/* <button className="count" >-</button>
//                 {this.state.count} <button className="count" onClick={
//                     this.incrementCount
//                 }>+</button> */}
//                 </div>
//             ))}
//             {/* </Grid> */}
//             </div>
           
//         </div>
//         )
//     }
// }

var count = 0  
// var newCount = 0   

function Left({setCount,setItems,setPrice,name}) {
    return (
        <div className="root">
            <div>
                <div>
                    <MenuIcon className="menuicons"/>
                </div>
                <div className="displayTitle">
                    {name}
                </div>
                <div>
                    <SearchIcon className="searchicons"/>
                </div>
            </div>
            <div className="datas">
            {
             Data.map((datas,values) =>(
                <div key={values}>
                <img src={datas.images}  className="imagesdata" onClick={ ()=>
                {
                    let amount =datas.price
                    let name =datas.name
                        // setItems(name)
                        setItems((names)=>[...names,name])
                        setPrice((tot) =>[...tot,amount])
                        setCount(
                            count=1
                        ) 
                        // console.log(`${name}${amount}`)
                    
                }}/>
                <strong>{datas.name}</strong> 
                </div>
            ))}
            </div>
           
        </div>
    )
}

export default Left

// export default Left
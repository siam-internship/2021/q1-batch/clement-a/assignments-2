import './App.css';
import Left from './Left'
import Right from './Right'
import React ,{useState,useEffect}from 'react'
import SplitPane, { Pane } from 'react-split-pane';
import './split.css'
import Footer from './Footer'

function App() {
  const [name,setName] = useState()
  const [count,setCount] = useState() 
    const [items , setItems] = useState([])
    const [price, setPrice] = useState([])

  return (
    <div className="App">
     <SplitPane split="vertical" minSize={1100}> 
      <div className="leftside">
        <Left setCount={setCount} setItems={setItems} setPrice={setPrice} name={name}/>
        <Footer setName={setName}/> 
      </div>
      {/* {console.log(items.length)} */}
      <div className="rightside" >
        <Right count={count} items={items} price={price}/>
      </div>
      </SplitPane> 
      
    </div>
  );
}

export default App;

import React,{useState,useEffect} from 'react'
import './data.json'
import './right.css'
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
// import jspdf from 'jspdf'
 // ES2015 module syntax
 import { PDFExport, savePDF } from '@progress/kendo-react-pdf';

let total=0;
function Right({count,items,price}) {
    const PdfExportMode = React.useRef(null);
    useEffect(
        () =>{
            price.map((itemsPrices)=>{
                total += itemsPrices;
            })
            console.log(total)
        } 
        ,[items || price]
            )

    const totals =() => {
        if(PdfExportMode.current){
            PdfExportMode.current.save();
        }
    }
    const changes= () =>{
        window.location.reload(false);
    } 
    return (
        <div className="roots">
            <div>
            <strong>Tickets</strong>
            <PersonAddIcon className="personadd"/>
            <MoreVertIcon className="morevert"/>
            </div>
            <div className="margin_one">
                <p className="margin_one_para">Dine in</p>
                <ArrowDropDownIcon className="arrow" />
                <div className="total">
                     {`Total ${total}`
                    } 

                </div>
                
            </div>
            <p className="lines"></p>
            
            <div className="items">
                <PDFExport ref={PdfExportMode} paperSize="auto" margin={20} fileName={`${new Date().getDate()}`}>
                 <ul>
                     {items.map((iter,values)=><strong key={values} >

                         <li>{iter} x {count} </li></strong>
                         
                         )}
                 </ul>
                 {`Total ${total}`}
                 </PDFExport>
            </div>
            
            <div className="tracker">
                <div className="saveButton" onClick={totals}>
                    <input type="button" value="Save" className="saveButton"/>
                </div>
                <div className="cancelButton"> 
                    <input type="button" value="Charge" onClick={changes} className="cancelButton"/>
                </div>
            </div>
        </div>
    )
}
export default Right
